package uz.azn.firebasecloudfirestore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.firebasecloudfirestore.databinding.RowItemViewHolderBinding as RowItemBinding

class RvAdapter : RecyclerView.Adapter<RvAdapter.RowItemViewHolder>() {
    private val elements = mutableListOf<User>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowItemViewHolder {
        return RowItemViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: RowItemViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class RowItemViewHolder(private val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(user: User) {
            with(binding) {
                tvFirstName.text = user.firstname
                tvLastName.text = user.lastname
            }
        }
    }
    fun setData(element:ArrayList<User>){
        elements.apply { clear(); addAll(element) }
        notifyDataSetChanged()
    }
}