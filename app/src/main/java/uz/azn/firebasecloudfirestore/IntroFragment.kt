package uz.azn.firebasecloudfirestore

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.firestore.FirebaseFirestore
import uz.azn.firebasecloudfirestore.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private lateinit var firebaseFirestore: FirebaseFirestore

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        firebaseFirestore = FirebaseFirestore.getInstance()
        with(binding) {
            btnSave.setOnClickListener {
                if (firstName.text.isNotEmpty() && lastName.text.isNotEmpty()) {
                    saveToFirestore(firstName.text.toString(), lastName.text.toString())
                }
            }
            btnUsers.setOnClickListener {
                fragmentManager!!.beginTransaction().replace(R.id.frame_layout, LoadFragment())
                    .commit()
            }
        }
    }

    private fun saveToFirestore(firsName: String, lastName: String) {
        val newUser: MutableMap<String, String> = HashMap()

        newUser["firstname"] = firsName
        newUser["lastname"] = lastName
        var user_id = 1
        firebaseFirestore.collection("users").get().addOnCompleteListener {
            if (it.isSuccessful) {
                for (document in it.result!!) {
                    user_id++
                }
                // qaysi collection ga joylashishni aytadi
                firebaseFirestore.collection("users")
                    .document("user_$user_id")
                    .set(newUser)
                    .addOnSuccessListener { documentReference ->
                        Toast.makeText(
                            requireContext(),
                            "Foydalanuvchi qoshildi ",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(
                            requireContext(),
                            "Hatollik yuz berdi ${it.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }

        }
    }

    private fun getChild(collection:String): Int {
        var count = 0
        firebaseFirestore.collection(collection).get().addOnCompleteListener {
            if (it.isSuccessful) {
                for (document in it.result!!) {
                    count++
                }
                with(binding) {
                }
            }
        }
        return count+1
    }
}