package uz.azn.firebasecloudfirestore

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.firebase.firestore.FirebaseFirestore
import uz.azn.firebasecloudfirestore.databinding.FragmentLoadBinding

class LoadFragment : Fragment(R.layout.fragment_load) {
    private lateinit var binding: FragmentLoadBinding
    private lateinit var firebaseFirestore: FirebaseFirestore
    private  var rvAdapter  =RvAdapter()
    private val listOfUsers = mutableListOf<User>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoadBinding.bind(view)
        firebaseFirestore = FirebaseFirestore.getInstance()
        readFromFirestore()

    }


    private fun readFromFirestore(){
        firebaseFirestore.collection("users").get().addOnCompleteListener{
            if (it.isSuccessful){
                binding.progressBar.visibility = View.INVISIBLE
                for (document in it.result!!){
                    val firstname = document.data.getValue("firstname") as String
                    val lastname = document.data.getValue("lastname") as String
                    listOfUsers.add(User(firstname,lastname))

                }
                with(binding){
                    rvAdapter.setData(listOfUsers as ArrayList<User>)
                    recyclerView.adapter = rvAdapter
                }
            }
        }
    }

}